#include<iostream>
#include"players.h"

int main(int argc, const char* argv[])
{
    int port;
    const char * ip;

    if (argc < 2)
    {
        std::cout << "Usage ./main hostname port";
        exit(1);
    }
    else
    {
        ip = argv[1];
        port = atoi(argv[2]);
    }

  int gameStatus = GS_NOTSTARTED;
  OnlinePlayer * Me = new OnlinePlayer(CLIENT, ip, port);
  Me->SetNick();

  while(gameStatus <= GS_PLAY)
  {
      Me->Display();
      gameStatus = Me->GetAction();
  }
  delete Me;
  return 0;
}
