#ifndef TCP_connection_h
#define TCP_connection_h

#include<stdio.h>
#include<string>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>

class TCP_connection
{
public:
    TCP_connection();
    ~TCP_connection();
    void error(const char *msg);
    int Set_TCP_Server(int port_number);
    int Set_TCP_Client(const char * server_ip, int port_number);
};

#endif
