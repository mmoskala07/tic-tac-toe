# Tic Tac Toe using TCP/IP

## Building
~~~{.sh}
. compiler.sh
~~~

## Executing
~~~{.sh}
./main 8888
./client localhost 8888
~~~

## Maintainer
Mateusz Moskała  
mmoskala07@gmail.com
