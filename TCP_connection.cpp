#include"TCP_connection.h"

TCP_connection::TCP_connection()
{}

TCP_connection::~TCP_connection()
{}

void TCP_connection::error(const char *msg)
{
    perror(msg);
    exit(1);
}

int TCP_connection::Set_TCP_Server(int port_number)
{
    int socket_h, connected_socket_h;
    struct sockaddr_in server_address, client_address;
    socklen_t client_len = sizeof(client_address);

    socket_h = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_h < 0)
        error("ERROR opening socket");
    else
    {
        printf("\n");
        system("echo IP: $(ifconfig | awk '/inet addr/{print substr($2,6)}' | grep --invert-match 127.0.0.1)");
        printf("Port: %d\n", port_number);
    }

    // Configure connection
    bzero((char*) &server_address, sizeof(server_address));
    server_address.sin_family = AF_INET; // Use IPv4
    server_address.sin_addr.s_addr = INADDR_ANY; // Bind to all available interfaces
    server_address.sin_port = htons(port_number); // Specify port number

    // Bind socet to connection
    if(bind(socket_h, (struct sockaddr *) &server_address, sizeof(server_address)) < 0)
        error("ERROR on binding");

    // Listen
    printf("Waiting for second player...\n");
    listen(socket_h, 10);

    // Accept
    connected_socket_h = accept(socket_h, (struct sockaddr *) &client_address, &client_len);
    if(connected_socket_h < 0)
        error("ERROR on accept");
    else
        printf("Connection established!\n");

    return connected_socket_h;
}

int TCP_connection::Set_TCP_Client(const char * server_ip, int port_number)
{
    int socket_h;
    struct sockaddr_in server_address;
    struct hostent *server; // defecines host address struct

    server = gethostbyname(server_ip); // get server address
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n"); // if server address is not proper
        exit(0); // exit main() function
    }

    // Create socket
    socket_h = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_h < 0)
        error("ERROR opening socket");

    // Config connection
    bzero((char *) &server_address, sizeof(server_address));
    server_address.sin_family = AF_INET; // define address family (IPv4)
    bcopy((char *)server->h_addr, (char *)&server_address.sin_addr.s_addr, server->h_length); // copy byte data
    server_address.sin_port = htons(port_number); // assign port

    // Connect
    if (connect(socket_h, (struct sockaddr *) &server_address, sizeof(server_address)) < 0)
        error("ERROR connecting"); // if connection failed
    else
        printf("Connection established!\n");

    return socket_h;
}
