#ifndef PLAYERS_H
#define PLAYERS_H

#include<iostream>
#include"global_enums.h"
#include"TCP_connection.h"

class Player
{
protected:
    std::string nick;
    SIGN sign;
    FIELD field[3][3];

public:
    // Constructor and Destructor
    Player();
    ~Player();

    // Methods
    virtual void Display(FIELD field[3][3]);
    virtual void Display(std::string info);

    // Acces to fields
    virtual bool SetNick();
    std::string GetNick() const;
    bool SetSign(SIGN sign);
    SIGN GetSign() const;
    unsigned int GetFieldNumber() const;
    virtual unsigned int GetAction() const;
};


enum CS {CLIENT, SERVER};

class OnlinePlayer : public Player
{
private:
    TCP_connection * TCP;
    CS socket_type;
    int socket_h;

public:
    // Constructor and Destructor
    OnlinePlayer(CS client_or_server, const char * ip, int port);
    ~OnlinePlayer();

    // Methods
    void Display();
    void Display(FIELD field[3][3]);
    void Display(std::string info);

    bool SetNick();
    unsigned int GetAction() const;
};

#endif
