#!/bin/bash

# Remove old main.exe
rm main
rm game.o
rm players.o
rm TCP_connection.o

# Compiler
g++ -c -std=c++11 TCP_connection.cpp
g++ -c -std=c++11 players.cpp
g++ -c -std=c++11 game.cpp
g++ main.cpp -std=c++11 -o main TCP_connection.o players.o game.o
g++ client.cpp -std=c++11 -o client players.o TCP_connection.o


# Wait
presence_main=$(find . -type f -name 'main' | wc --lines)
while [ $presence_main -eq 0 ]; do
  sleep 0.1
done

# Execute
