#include "game.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

Board::Board(int port)
{
  gameStatus = GS_NOTSTARTED;
  local_player = new Player();
  local_player->SetNick();
  remote_player = new OnlinePlayer(SERVER, "localhost", port);
  remote_player->SetNick();

  srand(static_cast<unsigned>(time(NULL)));
  if(rand() % 2 == 0)
  {
      local_player->SetSign(SGN_CROSS);
      remote_player->SetSign(SGN_CIRCLE);
  }
  else
  {
      local_player->SetSign(SGN_CIRCLE);
      remote_player->SetSign(SGN_CROSS);
  }
  (rand() % 2 == 0) ? actualPlayer = local_player : actualPlayer = remote_player;
}

Board::~Board()
{
  delete local_player;
  local_player = NULL;
  delete remote_player;
  remote_player = NULL;
}

void Board::Display()
{
  local_player->Display(field);
  remote_player->Display(field);
}

bool Board::InsertMovement()
{
  // Choose next player
  if(actualPlayer == local_player)
  {
      actualPlayer = remote_player;
      local_player->Display("Wait for enemy move...");
  }
  else
  {
      actualPlayer = local_player;
      remote_player->Display("Wait for enemy move...");
  }

  // Get data
  unsigned int fieldNumber = actualPlayer->GetAction();
  SIGN sign = actualPlayer->GetSign();

  // Conversion
  unsigned int x = (fieldNumber - 1) % 3;
  unsigned int y = (fieldNumber - 1) / 3;

  field[y][x] = static_cast<FIELD>(sign);

  return true;
}

void Board::CheckGameStatus()
{
  gameStatus = GS_PLAY;

  // Winning shapes
  const int LINIE[][3][2] =  { { { 0,0 }, { 0,1 }, { 0,2 } }, // górna pozioma
                               { { 1,0 }, { 1,1 }, { 1,2 } }, // środ. pozioma
                               { { 2,0 }, { 2,1 }, { 2,2 } }, // dolna pozioma
                               { { 0,0 }, { 1,0 }, { 2,0 } }, // lewa pionowa
                               { { 0,1 }, { 1,1 }, { 2,1 } }, // środ. pionowa
                               { { 0,2 }, { 1,2 }, { 2,2 } }, // prawa pionowa
                               { { 0,0 }, { 1,1 }, { 2,2 } }, // p. backslashowa
                               { { 2,0 }, { 1,1 }, { 0,2 } } }; // p. slashowa

  // Check if win
  for(int set=0; set<8; set++)
  {
    // Examine first sign
    int y = LINIE[set][0][0];
    int x = LINIE[set][0][1];
    FIELD pattern = field[y][x];
    // Test rest signs in field
    for(int i=1; i<3; i++)
    {
      int y = LINIE[set][i][0];
      int x = LINIE[set][i][1];
      if(pattern != field[y][x])
        break;
      if(i == 2 && pattern != FLD_EMPTY)
        gameStatus = GS_WON;
    }
    if(gameStatus == GS_WON)
      break;
  }
  // Check if draw
  int emptyFields = 0;
  for(int y=0; y<3; y++)
    for(int x=0; x<3; x++)
      if(field[y][x] == FLD_EMPTY)
        emptyFields++;
  if(emptyFields == 0)
    gameStatus = GS_DRAW;

}

void Board::AnnounceResult()
{
  switch(gameStatus)
  {
    case GS_WON:
        actualPlayer->Display("You win!");
        if(actualPlayer == local_player)
            remote_player->Display("You lose :(");
        else
            local_player->Display("You lose :(");
        break;
    case GS_DRAW:
        local_player->Display("You draw! :|");
        remote_player->Display("You draw! :|");
        break;
  }
}
