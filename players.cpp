#include"players.h"

Player::Player()
{

}

Player::~Player()
{

}

void Player::Display(FIELD field[3][3])
{
    system("clear");
    std::cout << "     RING AND CROSS     " << std::endl;
    std::cout << "------------------------" << std::endl;
    std::cout << std::endl;

    std::cout << "      - - - - - " << std::endl;
    for(int i=0; i<3; i++)
    {
      std::cout << "      | ";
      for(int j=0; j<3; j++)
      {
        if(field[i][j] == FLD_EMPTY)
          std::cout << i*3 + j+1 << " ";
        else
          std::cout << static_cast<char>(field[i][j]) << " ";
      }
      std::cout << "|" << std::endl;
    }
    std::cout << "      - - - - - " << std::endl;
    std::cout << std::endl;
}

void Player::Display(std::string info)
{
    std::cout << info << std::endl;
}

bool Player::SetNick()
{
  std::string nick;
  std::cout << "Enter your nick: ";
  getline(std::cin, nick);
  this->nick = nick;
  std::cout << "Hi " << nick << "!" << std::endl;
  return true;
}

std::string Player::GetNick() const
{
  return nick;
}

bool Player::SetSign(SIGN sign)
{
  this->sign = sign;
  return true;
}

SIGN Player::GetSign() const
{
  return sign;
}

unsigned int Player::GetFieldNumber() const
{
    unsigned int fieldNumber;
    while(true)
    {
        std::cout << "Enter field number: ";
        std::cin >> fieldNumber;

        if(!std::cin.fail() && fieldNumber >= 0 && fieldNumber <= 9)
        {
            // Conversion
            unsigned int x = (fieldNumber - 1) % 3;
            unsigned int y = (fieldNumber - 1) / 3;
            if(field[y][x] == FLD_EMPTY)
                break;
        }
        else
        {
            std::cin.clear();
            std::cin.ignore(999, '\n');
        }
    }
    return fieldNumber;
}

unsigned int Player::GetAction() const
{
  std::cout << nick << " you play by ";
  switch(sign)
  {
    case SGN_CROSS:
      std::cout << "crosses" << std::endl;
      break;
    case SGN_CIRCLE:
      std::cout << "circles" << std::endl;
      break;
  }

  return GetFieldNumber();
}



OnlinePlayer::OnlinePlayer(CS client_or_server, const char * ip, int port)
{
    TCP = new TCP_connection();

    if(client_or_server == CLIENT)
    {
        socket_h = TCP->Set_TCP_Client(ip, port);
        socket_type = CLIENT;
    }
    else if(client_or_server == SERVER)
    {
        socket_h = TCP->Set_TCP_Server(port);
        socket_type = SERVER;
    }
}

OnlinePlayer::~OnlinePlayer()
{
    close(socket_h);
    delete TCP;
    TCP = NULL;
}

void OnlinePlayer::Display()
{
    // Read
    // FIELD read_field[3][3];
    int read_bytes = read(socket_h, Player::field, 36);
    if (read_bytes < 0)
        TCP->error("ERROR reading from client");
    else
        std::cout << "Readed: " << read_bytes << std::endl;

    Player::Display(field);
}

void OnlinePlayer::Display(FIELD field[3][3])
{
    // Write
    int written_bytes = write(socket_h, field, 36);
    if(written_bytes < 0)
        TCP->error("ERROR writing to client");
}

void OnlinePlayer::Display(std::string info)
{
    // Write
    int written_bytes = write(socket_h, info.c_str(), strlen(info.c_str()));
    if(written_bytes < 0)
        TCP->error("ERROR writing to client");
}

bool OnlinePlayer::SetNick()
{
    if(socket_type == SERVER)
    {
        // Write
        char data[19] = "Enter your nick: \0";
        int written_bytes = write(socket_h, data, strlen(data));
        if(written_bytes < 0)
            TCP->error("ERROR writing to client");
        else
            std::cout << "Send request" << std::endl;

        // Read
        char nick[256];
        bzero(nick, 256);
        int read_bytes = read(socket_h, nick, 256);
        if (read_bytes < 0)
            TCP->error("ERROR reading from client");
        this->nick = nick;
        std::cout << "Nick of your opponent is: " << nick << std::endl;

    }
    else if(socket_type == CLIENT)
    {
        // Read
        char data[256];
        bzero(data, 256);
        int read_bytes = read(socket_h, data, 256);
        if (read_bytes < 0)
            TCP->error("ERROR reading from client");
        else
            std::cout << data;

        // Write
        char nick[256];
        bzero(nick, 256);
        fgets(nick, 256, stdin);
        nick[strlen(nick)-1] = '\0';
        this->nick = nick;
        int written_bytes = write(socket_h, nick, strlen(nick));
        if(written_bytes < 0)
            TCP->error("ERROR writing to client");
    }
}

unsigned int OnlinePlayer::GetAction() const
{
    if(socket_type == SERVER)
    {
        std::string label = nick + " you play by ";
        switch(sign)
        {
          case SGN_CROSS:
            label += "crosses";
            break;
          case SGN_CIRCLE:
            label += "circles";
            break;
        }
        label += "\nEnter field number: ";

        // Write
        int written_bytes = write(socket_h, label.c_str(), strlen(label.c_str()));
        if(written_bytes < 0)
            TCP->error("ERROR writing to client");

        // Read
        int fieldNumber;
        int read_bytes = read(socket_h, &fieldNumber, 4);
        if (read_bytes < 0)
            TCP->error("ERROR reading from client");

        std::cout << "RECEIVED: " << fieldNumber << std::endl;

        return fieldNumber;
    }
    else if(socket_type == CLIENT)
    {
        // Read
        char buffer[256];
        bzero(buffer, 256);
        int read_bytes = read(socket_h, buffer, 255);
        if (read_bytes < 0)
            TCP->error("ERROR reading from client");

        if(strlen(buffer) == strlen("Wait for enemy move...\0"))
        {
            std::cout << "Wait for enemy move..." << std::endl;
            return GS_PLAY;
        }
        else if(strlen(buffer) == strlen("You draw! :|\0"))
        {
            std::cout << "Draw! :|" << std::endl;
            return GS_DRAW;
        }
        else if(strlen(buffer) == strlen("You win!\0"))
        {
            std::cout << "You win!" << std::endl;
            return GS_WON;
        }
        else if(strlen(buffer) == strlen("You lose :(\0"))
        {
            std::cout << "You lose :(" << std::endl;
            return GS_LOSE;
        }
        else
        {
            // Write
            int fieldNumber = Player::GetFieldNumber();
            int written_bytes = write(socket_h, &fieldNumber, 4);
            if(written_bytes < 0)
                TCP->error("ERROR writing to client");
            return 1;
        }
    }
}
