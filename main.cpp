#include<iostream>
#include"game.h"

int main(int argc, const char* argv[])
{
  int port;

  if (argc < 2)
  {
      std::cout << "Usage ./main port";
      exit(1);
  }
  else
      port = atoi(argv[1]);


  Board * RingCross = new Board(port);

  while(true)
  {
    RingCross->Display();
    RingCross->CheckGameStatus();

    if(RingCross->gameStatus == GS_PLAY)
      RingCross->InsertMovement();
    else
    {
      RingCross->AnnounceResult();
      break;
    }
  }
  delete RingCross;
  return 0;
}
