#ifndef GAME_H
#define GAME_H
#include<iostream>
#include"global_enums.h"
#include"players.h"

class Board
{
  private:
    FIELD field[3][3] = { {FLD_EMPTY, FLD_EMPTY, FLD_EMPTY},
                          {FLD_EMPTY, FLD_EMPTY, FLD_EMPTY},
                          {FLD_EMPTY, FLD_EMPTY, FLD_EMPTY} };
    Player * local_player;
    OnlinePlayer * remote_player;
    Player * actualPlayer;
  public:
    GAMESTATE gameStatus;

  public:
    // Constructor and Destructor
    Board(int port);
    ~Board();

    // Methods
    void Display();
    bool InsertMovement();
    void CheckGameStatus();
    void AnnounceResult();
};

#endif
